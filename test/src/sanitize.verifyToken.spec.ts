"use strict";
import * as chai from 'chai';
const expect = chai.expect;
import f from '../../js/functions/sanitize';
const v = f.verifyToken;

describe('valid tokens - expect true', () => {
    it('gbw54w54.4565234f.b4wh4w5', () => {
        let n = "gbw54w54.4565234f.b4wh4w5";
        return expect(v(n)).to.equal(true);
    });
    it('egurgheiurghei.nbiewbrog.nineqfefww', () => {
        let n = 'egurgheiurghei.nbiewbrog.nineqfefww';
        return expect(v(n)).to.equal(true);
    });
    it('eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJNaWtvxYJhaiIsImxhc3ROYW1lIjoiQ2Fua3VkaXMiLCJlbWFpbCI6Im1pa29sYWpAY2Fua3VkaXMubmV0IiwiYmlydGhfZGF0ZSI6IjIwMTYtMDQtMjkiLCJ0c2hpcnRfdHlwZSI6Im0iLCJ0c2hpcnRfc2l6ZSI6Ik0iLCJzdWIiOiJsYXE1NjdxcDRrZzFwbTYwMGJpcDVnb2VvbzJkbDVwcTlwczhybXNraTdjZmhsc2NtZDh0b2twbzhxNHNjOWRzcXMyNTYyOW9yOTNlYW9ybWc0NDg1amxqMmI3bzZyaTMiLCJpYXQiOjE1NTEzMDM4MzAsImV4cCI6MTU1MTMwNzQzMH0.bMZI6xcjIZeA4mygb2zp_MgmRlevO29yMRNd7BJzMu9cGeb8IIXCHjvIJrmwaT_4SSfbcNnbeWcxd0nmi6VZYg', () => {
        let n = 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJNaWtvxYJhaiIsImxhc3ROYW1lIjoiQ2Fua3VkaXMiLCJlbWFpbCI6Im1pa29sYWpAY2Fua3VkaXMubmV0IiwiYmlydGhfZGF0ZSI6IjIwMTYtMDQtMjkiLCJ0c2hpcnRfdHlwZSI6Im0iLCJ0c2hpcnRfc2l6ZSI6Ik0iLCJzdWIiOiJsYXE1NjdxcDRrZzFwbTYwMGJpcDVnb2VvbzJkbDVwcTlwczhybXNraTdjZmhsc2NtZDh0b2twbzhxNHNjOWRzcXMyNTYyOW9yOTNlYW9ybWc0NDg1amxqMmI3bzZyaTMiLCJpYXQiOjE1NTEzMDM4MzAsImV4cCI6MTU1MTMwNzQzMH0.bMZI6xcjIZeA4mygb2zp_MgmRlevO29yMRNd7BJzMu9cGeb8IIXCHjvIJrmwaT_4SSfbcNnbeWcxd0nmi6VZYg';
        return expect(v(n)).to.equal(true);
    });
});
describe('invalid tokens - expect false', () => {
    it('two dots next to each other', () => {
        let n = "ab..cd";
        return expect(v(n)).to.equal(false);
    });
    it('not enough dots', () => {
        let n = "abcd";
        return expect(v(n)).to.equal(false);
    });
    it('too many dots', () => {
        let n = "aa.bb.cc.dd";
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign space', () => {
        let n = "ab cd";
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign *', () => {
        let n = "ab*cd";
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign (', () => {
        let n = 'si(gn';
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign ,', () => {
        let n = "si,gn";
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign ;', () => {
        let n = "some;thing";
        return expect(v(n)).to.equal(false);
    });
    it("illegal sign '", () => {
        let n = "some'thing";
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign "', () => {
        let n = 'some"thing';
        return expect(v(n)).to.equal(false);
    });
});
