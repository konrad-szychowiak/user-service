"use strict";
import * as chai from 'chai';
const expect = chai.expect;
import f from '../../js/functions/sanitize';
const v = f.verifyWord;

describe('valid words - expect true', () => {
    it('5ec43rqrgiuabistggt4tb09zfb', () => {
        let n = "5ec43rqrgiuabistggt4tb09zfb";
        return expect(v(n)).to.equal(true);
    });
    it('camelCaseName', () => {
        let n = 'camelCaseName';
        return expect(v(n)).to.equal(true);
    });
});
describe('invalid words - expect false', () => {
    it('illegal sign space', () => {
        let n = "ab cd";
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign *', () => {
        let n = "ab*cd";
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign (', () => {
        let n = 'si(gn';
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign ,', () => {
        let n = "si,gn";
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign _', () => {
        let n = "some_thing";
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign -', () => {
        let n = "some-thing";
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign .', () => {
        let n = "some.thing";
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign ;', () => {
        let n = "some;thing";
        return expect(v(n)).to.equal(false);
    });
    it("illegal sign '", () => {
        let n = "some'thing";
        return expect(v(n)).to.equal(false);
    });
    it('illegal sign "', () => {
        let n = 'some"thing';
        return expect(v(n)).to.equal(false);
    });
});
