"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const sanitize_1 = require("../functions/sanitize");
exports.getRandomString = (mini, maxi) => {
    let tmp = Math.ceil(Math.random() * (maxi - mini)) + mini;
    let helper = "1234567890abcdefghijklmnopqrstuvwxyz";
    let string = '';
    for (let i = 0; i < tmp; i++) {
        let rand = Math.floor(Math.random() * 30);
        string += helper[rand];
    }
    return string;
};
exports.login = ((username, password, userDependency) => __awaiter(this, void 0, void 0, function* () {
    try {
        if (!username || !password)
            throw { msg: 'Missing Credentials' };
        if (!sanitize_1.default.verifyString(username))
            throw { msg: 'Invalid Data' };
        if (!sanitize_1.default.verifyString(password))
            throw { msg: 'Invalid Data' };
        const user = yield userDependency.getByUsername(username);
        const isMatch = yield userDependency.comparePassword(password, user.password);
        if (!isMatch)
            throw { msg: 'Wrong Password' };
        return user;
    }
    catch (err) {
        console.error('[FUNCTIONS/MAIN/LOGIN]', err);
        throw err;
    }
}));
