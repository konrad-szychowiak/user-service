'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
dotenv.config();
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const helmet = require("helmet");
// Security headers
app.use(helmet());
// Body parse
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// These are the routes
const user_1 = require("./routes/user");
app.use('/user', user_1.default);
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(new Date, `Hi, listening on port ${port}`);
});
