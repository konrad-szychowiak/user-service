"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sanitize_1 = require("../functions/sanitize");
const jwt = require("jsonwebtoken");
exports.ensureAuthenticated = (req, res, next) => {
    if (!sanitize_1.default.verifyString(req.headers['x-auth']))
        return res.status(403).send("Missing credentials");
    if (req.headers['x-auth']) {
        jwt.verify(req.headers['x-auth'], process.env.SECRET, function (err, token) {
            if (err) {
                res.status(403).send("Permission denied");
                return false;
            }
            else {
                req.auth = token;
                return next();
            }
        });
    }
    else {
        res.sendStatus(403);
        return false;
    }
};
