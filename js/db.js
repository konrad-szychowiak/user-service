"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const dbpath = process.env.DB_PATH || 'mongodb://localhost/loginservice';
mongoose.connect(dbpath, { user: process.env.DB_USER, pass: process.env.DB_PASS, useNewUrlParser: true }, err => {
    if (err)
        console.error(`[${new Date}, db/connect]`, err);
    else
        console.log(new Date, `Nawiązano połączenie z bazą`);
});
exports.default = mongoose;
