'use strict';
import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import * as nodemailer from 'nodemailer';
const router = express.Router();

// DB Models
import {User, TokenAuth} from '../models/user';

// Middleware/functions
import sanitize from '../functions/sanitize';
import {getRandomString, login} from '../functions/main'

router.post('/register', (req,res) => {
  let email = req.body.email;
  let username = req.body.username;
  let password = req.body.password;
  if(!sanitize.verifyEmail(email)) return res.status(400).send("Invalid data");
  if(!sanitize.verifyString(username)) return res.status(400).send("Invalid data");
  if(!sanitize.verifyString(password)) return res.status(400).send("Invalid data");
  let newUser = new User({
    email: email,
    username: username,
    password: password,
    reissue_id: getRandomString(20, 30)
  });
  User.createUser(newUser)
  .then(user => res.status(200).send("Account created"))
  .catch(err => {
    if(err.code == "11000") {
      if(err.errmsg.includes("email")) return res.status(404).send("Email already taken");
      return res.status(404).send("Username already taken");
    }
    else return res.status(400).send("Unknown error");
  })
});

router.post('/login',
async (req, res) => {
  try {
    if(!sanitize.verifyString(req.body.username)) throw {msg: "Invalid data"};
    if(!sanitize.verifyString(req.body.password)) throw {msg: "Invalid data"};
    let user = await login(req.body.username, req.body.password, new User);
    let random = getRandomString(40, 60);
    let token = jwt.sign({
      username: user.username,
      id: user._id,
      random: random
    }, process.env.SECRET, {algorithm: 'HS512', expiresIn: '10m'});
    random = getRandomString(40, 60);
    let reissueToken = jwt.sign({
      id: user._id,
      random: random,
      reissue_id: user.reissue_id
    }, process.env.REISSUE_SECRET, {algorithm: 'HS512', expiresIn: '7d'});
    res.json({
      token,
      reissueToken,
      user: {
        username: user.username,
        _id: user._id
      }
    });
  }
  catch(err) {
    console.error(`[${new Date}, /user/login]`, err);
    if(err.msg) return res.status(400).send(err.msg);
    res.sendStatus(400);
  }
})

router.get('/id/:id', (req, res) => {
  let id = req.params.id;
  if(!sanitize.verifyWord(id)) return res.status(400).send("Invalid data");
  User.findOne({_id: id}).select('username').select('_id').select('reissue_id').exec((err, user) => {
    if(err) return res.sendStatus(404);
    if(!user) return res.sendStatus(404);
    res.json(user);
  })
})

router.get('/username/:name', async (req, res) => {
  let username = req.params.name;
  if(!sanitize.verifyString(username)) return res.status(400).send("Invalid data");
  User.findOne({username: username}).select('username').select('_id').select('reissue_id').exec((err, user) => {
    if(err) return res.sendStatus(404);
    if(!user) return res.sendStatus(404);
    res.json(user);
  })
})

router.post('/password/change', async(req,res) => {
  try {
    if(!req.body.username || !req.body.password || !req.body.newPassword) throw {msg: "Missing data"}
    if(!sanitize.verifyString(req.body.username)) throw {msg: "Invalid data"};
    if(!sanitize.verifyString(req.body.password)) throw {msg: "Invalid data"};
    if(!sanitize.verifyString(req.body.newPassword)) throw {msg: "Invalid data"};
    let user = await login(req.body.username, req.body.password, new User);
    let result = await User.changePassword(user._id, req.body.newPassword);
    console.log(result);
    return res.status(200).send("Password changed");
  }
  catch(err) {
    console.error(`[${new Date}, /USER/PASSWORD/CHANGE]`, err);
    if(err.msg) return res.status(400).send(err.msg);
    res.sendStatus(400);
  }
})

router.post('/username/change', async(req,res) => {
  try {
    if(!sanitize.verifyString(req.body.username)) return res.status(400).send("Invalid data");
    if(!sanitize.verifyString(req.body.password)) return res.status(400).send("Invalid data");
    if(!sanitize.verifyString(req.body.newUsername)) return res.status(400).send("Invalid data");
    let user = await login(req.body.username, req.body.password, new User);
    let result = await User.changeUsername(user._id, req.body.newUsername);
    console.log(result);
    return res.status(200).send("Username changed");
  }
  catch(err) {
    console.error(`[${new Date}, /USER/USERNAME/CHANGE]`, err);
    if(err.code===11000) err.msg = "Username already taken";
    if(err.msg) return res.status(400).send(err.msg);
    res.sendStatus(400);
  }
})

router.post('/password/email', async(req, res) => {
  try {
    if(!sanitize.verifyString(req.body.email)) return res.status(400).send("Invalid data");
    let user = await User.findOne({email: req.body.email});
    if(!user) return res.status(404).send("Account not found");
    let random = getRandomString(40, 100);
    let reset_id = getRandomString(40, 100);
    let token = jwt.sign({ username: user.username, id: user._id, random: random, reset_id: reset_id},process.env.RESET_SECRET, {expiresIn: '5m'});
    let result = await User.updateOne({_id: user._id}, {$set: {reset_id: reset_id}});
    let transporter = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: 587,
      secure: false,
      auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASSWORD
      },
      tls: {
        rejectUnauthorized: false
      }
    });
    let text =
      `<b>Hello ${user.username}</b>
      <p>Someone, probably You, requested a password reset for Your potikreff account.
        Click the link below to enter a new password. Please notice that the link is only active for 5 minutes.</p>
      <a href="${process.env.SERVER_PATH}/reset/${token}">Password reset</a>
      <p>In case it was not You who requested a password reset, please contact our support at
        <a href="mailto:mikolaj@cankudis.net">mikolaj@cankudis.net</a></p>`;
    let subject = "Password reset";
    let mailOptions = {
      from: '"noreply" <noreply@accounts.potikreff.pl>',
      to: req.body.email,
      subject: subject,
      html: text
    };
    let transport = await transporter.sendMail(mailOptions);
    console.log(`[${new Date}, transport]`, transport);
    res.sendStatus(200);
  }
  catch(err) {
    console.error(`[${new Date}, /token/resetPassword]`, err);
    if(err.msg) return res.status(400).send(err.msg);
    res.sendStatus(400);
  }
})

router.post('/password/reset', async(req,res) => {
  try {
    if(!sanitize.verifyString(req.body.password) || !req.body.password) return res.status(400).send("Invaid data");
    if(!sanitize.verifyString(req.body.token) || !req.body.token) return res.status(400).send("Invaid data");
    let tokenData: TokenAuth | string = await jwt.verify(req.body.token, process.env.RESET_SECRET);
    if(typeof tokenData === 'string') throw {msg: "Missing credentials"};
    let user = await User.getById(tokenData._id);
    if(user.reset_id!==tokenData.reset_id) return res.status(400).send("Invaid data");
    let result = await User.changePassword(tokenData._id, req.body.password);
    console.log(`[${new Date}, result]`, result);
    result = await User.updateOne({_id: user._id}, {$set: {reset_id: ''}});
    console.log(`[result2]`, result);
    res.status(200).send("Password changed");
  }
  catch(err) {
    console.error(`[${new Date}, /user/password/reset]`, err);
    if(err.msg) return res.status(400).send(err.msg);
    res.sendStatus(400);
  }
})
export default router;
