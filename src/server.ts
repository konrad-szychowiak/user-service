'use strict';
import * as dotenv from 'dotenv';
dotenv.config();
import * as express from 'express';
const app = express();
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';

// Security headers
app.use(helmet());

// Body parse
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// These are the routes
import userRoute from './routes/user';
app.use('/user', userRoute);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(new Date, `Hi, listening on port ${port}`);
})
