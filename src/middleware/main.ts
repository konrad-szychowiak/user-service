import sanitize from '../functions/sanitize';
import * as jwt from 'jsonwebtoken';

export const ensureAuthenticated = (req, res, next) => {
  if(!sanitize.verifyString(req.headers['x-auth'])) return res.status(403).send("Missing credentials");
  if(req.headers['x-auth']) {
    jwt.verify(req.headers['x-auth'], process.env.SECRET, function(err, token) {
      if(err) {
        res.status(403).send("Permission denied");
        return false;
      }
      else {
        req.auth = token;
        return next();
      }
    });
  } else {
    res.sendStatus(403);
    return false;
  }
}
